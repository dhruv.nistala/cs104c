\documentclass{beamer}
\usepackage{amsmath,amsthm,amsfonts,amssymb,amscd}
\usepackage{lastpage}
\usepackage{enumerate}
\usepackage{fancyhdr}
\usepackage{mathrsfs}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{mymacros}
\usepackage{tikz}
\usetikzlibrary{cd}
\usetheme{Madrid}
\usepackage{verbatim}
\usepackage{verbatimbox}
\usepackage{fancyvrb}

% PUT YOUR MACROS IN mymacros.sty
\hypersetup{
  colorlinks=true,
  linkcolor=blue,
  linkbordercolor={0 0 1}
}
 
\renewcommand\lstlistingname{Algorithm}
\renewcommand\lstlistlistingname{Algorithms}
\def\lstlistingautorefname{Alg.}
\makeatletter
\newcommand*{\rom}[1]{\expandafter\@slowromancap\romannumeral #1@}
\makeatother
\lstdefinestyle{Python}{
    language        = Python,
    frame           = lines, 
    basicstyle      = \footnotesize,
    keywordstyle    = \color{blue},
    stringstyle     = \color{green},
    commentstyle    = \color{red}\ttfamily
}

\title{Number Theory}
\author{Zeki Gurbuz}
\institute{CS 104C}
\date{Spring 2023}

\begin{document}

\frame{\titlepage}

\begin{frame}{Introduction}

    \begin{block}{Definitions}
        Throughout, $\Z$ are the integers and $\N$ the natural numbers. Recall that $\Z = \{\dots, -2, -1, 0, 1, 2 \dots\}$ whereas $\N = \{1, 2, \dots\}$\footnote{Some define $\N = \{0, 1, 2, \dots\}$, which is slightly problematic for our discussion.}.
    \end{block}
    
    \begin{block}{Divisibility}
        For $a, b \in \Z$, we say that $a \mid b$ ($a$ ``divides'' $b$) if there exists some $k \in \Z$ such that $b = ak$. Here we say that $a$ is a \textbf{divisor} of $b$.
        \begin{itemize}
            \item \textit{Examples}: $2 \mid 8$, as $8 = 2 \cdot 4$. $3 \nmid 10$ (proof by contradiction).
        \end{itemize}
    \end{block}
    
    \begin{block}{Division with Remainder}
        The \textbf{division algorithm} tells us that for any $a, b \in \Z$, there exist unique $q, r \in \Z$ such that $a = bq + r$ and $0 \leq r < b$.
        \begin{itemize}
            \item \textit{Remark}: If $b \mid a$, then $r = 0$, otherwise $r = a$ ``\%'' $b$.
        \end{itemize}
    \end{block}

\end{frame}

\begin{frame}[fragile=singleslide]{Greatest Common Divisor, Least Common Multiple}

    \begin{block}{Definitions}
        \begin{itemize}
            \item The \textbf{GCD} of $a, b \in \N$ is the \textit{largest} $d \in \N$ such that $d \mid a, d \mid b$.
            \item The \textbf{LCM} of $a, b \in \N$ is the \textit{smallest} $d \in \N$ such that $a \mid d, b \mid d$.
            \item \textit{Remark}: For all $a, b \in \N$, $\text{gcd}(a, b) \cdot \text{lcm}(a, b) = a \cdot b$.
            \item If $\text{gcd}(a, b) = 1$, we say that $a$ and $b$ are \textbf{coprime} (\textbf{relatively prime}).
        \end{itemize}
    \end{block}

    \begin{block}{Euclidean Algorithm}
        \begin{itemize}
            \item If $a = qb + r$ and $g \mid a, g \mid b$ for some $g \in \N$, then $g \mid r$.
            \item \textit{Proof}: $r = a - qb$ and $g \mid a, g \mid b$, so $g \mid r$. $\square$
        \end{itemize}
        \verb!int gcd(int a, int b) {return b == 0 ? a : gcd(b, a % b);}!
        \begin{itemize}
            \item Time Complexity: $\calO(\log{(a + b)})$.
            \item \textit{Puzzle}: Which inputs exhibit the worst-case behavior?
        \end{itemize}
    \end{block}

\end{frame}

\begin{frame}[fragile=singleslide]{Primality}

    \begin{block}{Definition}
        For $n \in \N$, we say $n$ is \textbf{prime} if $n$ has exactly two divisors in $\N$.
        \begin{itemize}
            \item \textit{Remark}: $1 \mid n, n \mid n$, so prime $n$ has no \textbf{proper} divisors.
        \end{itemize}
        The first few primes are $2, 3, 5, 7, 11, 13, 17, 19, 23, \dots$
    \end{block}

    \begin{block}{Why Primes?}
        The \textbf{Fundamental Theorem of Arithmetic} states that every number $n \in \N \setminus \{1\}$ has a \textbf{unique} representation (``prime factorization'') $n = p_1^{e_1} \cdot p_2^{e_2} \cdot \ldots \cdot p_k^{e_k}$ for primes $p_1, \dots, p_k$.
        \begin{itemize}
            \item \textit{Remark}: $n$ has exactly $\prod_{i=1}^k{(e_i + 1)}$ positive divisors (Why?).
            \item \textit{Discuss}: Given two factorizations, how can we recover GCD? LCM? 
            \item In short, it seems intuitive that primes exhibit some useful properties.
        \end{itemize}
    \end{block}
    
\end{frame}

\begin{frame}[fragile=singleslide]{Primality Checking}

    \begin{block}{Problem}
        Given $n \in \N$ ($1 \leq n \leq 10^{12}$), determine whether or not $n$ is prime.
    \end{block}

    \begin{block}{Na\"{i}ve Approach}
        \begin{verbatim}
boolean isPrime(int n) {
    for (int d = 2; d < n; ++d)
        if (n % d == 0) // d | n
            return false;
    return true;
}\end{verbatim}
        \begin{itemize}
            \item Time Complexity: $\calO(n)$.
            \item \textit{Discuss}: Can we improve this?
        \end{itemize}
    \end{block}

\end{frame}

\begin{frame}[fragile=singleslide]{Primality Checking (cont.)}

    \begin{block}{Faster Solution}
        \begin{verbatim}
boolean isPrime(int n) {
    for (int d = 2; d * d <= n; ++d)
        if (n % d == 0) // d | n
            return false;
    return true;
}\end{verbatim}
        \begin{itemize}
            \item \textit{Discuss}: Does this still work? If so, why?
            \item Time Complexity: $\calO(\sqrt{n})$, as $d^2 \leq n \implies d \leq \sqrt{n}$.
        \end{itemize}
    \end{block}

    \begin{block}{Other Methods}
        \begin{itemize}
            \item Probabilistic (Fermat \& Miller-Rabin primality tests).
            \item Used in Java's \verb!BigInteger.isProbablePrime!, though not so common in competitive programming.
        \end{itemize}
    \end{block}

\end{frame}

\begin{frame}[fragile=singleslide]{Sieve of Eratosthenes}

    \begin{block}{Problem}
        Given $n \in \N$ ($1 \leq n \leq 10^5$), output all primes $p$ such that $p \leq n$.
    \end{block}
    \begin{block}{Solution}
        \begin{itemize}
            \item Assume all natural numbers (greater than $1$) are prime. Iterative over $i \in \N, i \leq n$. If $i$ is considered prime, mark $2 \cdot i, 3 \cdot i, \dots$ as composite.
        \end{itemize}
        \begin{verbnobox}[\fontsize{8pt}{8pt}\selectfont]
void sieve(int n) {
    boolean composite = new boolean [n + 1];
    for (int i = 2; i <= n; ++i) {
        if (composite[i])
            continue;
        System.out.println(i + " is prime.");
        for (int j = i * 2; j <= n; j += i)
            composite[j] = true;
    }
}\end{verbnobox}
    \begin{itemize}
        \item Time Complexity: $\calO(n\log{\log{n}})$ (analyze asymptotics of $\sum\limits_{\substack{p \text{ prime}}}\frac{1}{p}$).
    \end{itemize}
    \end{block}

\end{frame}

\begin{frame}{Motivation for Modular Arithmetic}

    \begin{block}{Problems}
        \begin{itemize}
            \item Given $n \in \N$ ($1 \leq n \leq 10$), output $n!$.
            \item Given $n \in \N$ ($1 \leq n \leq 10$), output the probability that flipping $n$ fair coins yields $n$ ``heads''
            \item \textit{Discuss}: What if, instead, $1 \leq n \leq 10^6$?
        \end{itemize}
    \end{block}

    \begin{block}{``Output the answer modulo $10^9 + 7$''}
        \begin{itemize}
            \item $10^9 + 7$ is prime. We are working in a \textbf{finite field}, $\F_{10^9 + 7}$.
            \item So, we can add, subtract, multiply, and ``\textbf{divide}'' (more on this later).
            \item Also, it fits in a $32$-bit signed integer ($10^9 + 7 \leq 2^{31} - 1 \approx 2 \cdot 10^9$).
            \item \textit{Discuss}: We are no longer outputting the \textit{true} answer to the problem. However, this is not a problem in practice. How can that be the case?
            \item We now shift our discussion to ``how to work modulo a prime''.
        \end{itemize}
    \end{block}

\end{frame}


\begin{frame}[fragile=singleslide]{Modular Arithmetic}

    \begin{block}{Equivalence}
        \begin{itemize}
            \item If we fix a modulus, $n$, then having the same remainder upon division by $n$ is an \textbf{equivalence relation}.
            \begin{itemize}
                \item \textit{Examples: $8 \equiv 3 \modu{5}, 12 + 8 \equiv 0 \modu{10}$}
            \end{itemize}
            \item This captures the same information as division with remainder:
            \begin{itemize}
                \item $a \mid b \iff b \equiv 0 \modu{a}$.
                \item $b = qa + r \iff a \mid (b - r) \iff b \equiv r \modu{a}$
            \end{itemize}
        \end{itemize}
    \end{block}

    \begin{block}{Operating modulo $p$}
        \begin{itemize}
            \item In practice, we make use of the \verb+%+ (modulo) operator:
            \begin{itemize}
                \item \verb+%+ has the same precedence as \verb+*+ and \verb+/+ in most languages.
                \item \verb!(a + b) % p = ((a % p) + (b % p)) % p!
                \item \verb!(a - b) % p = (((a % p) - (b % p)) % p + p) % p!
                \item \verb!(a * b) % p = ((a % p) * (b % p)) % p! (\verb!long long!?)
                \item \verb+a^b % p = (a % p)^b % p != (a % p)^(b % p) % p+
            \end{itemize}
        \end{itemize}
    \end{block}

\end{frame}

\begin{frame}[fragile=singleslide]{Problems Revisited}
    \begin{block}{Problem}
        Given $n \in \N$ ($1 \leq n \leq 10^6$), output $n!$ modulo $10^9 + 7$.
    \end{block}

    \begin{block}{Solution}
        Recall that $n! = 1 \cdot \ldots \cdot n$, so we can multiply each term, taking the result modulo $10^9 + 7$ after each multiplication.
        \begin{verbatim}
int factorial(int n) {
    int MOD = (int) (1e9 + 7);
    long ans = 1;
    for (int i = 2; i <= n; ++i)
        ans = (ans * i) % MOD;
    return (int) ans;
}\end{verbatim}
    \end{block}
\end{frame}

\begin{frame}[fragile=singleslide]{Problems Revisited}
    \begin{block}{Problem}
        Given $n \in \N$ ($1 \leq n \leq 10^6$), output the probability that flipping $n$ fair coins yields $n$ ``heads''. If the answer is $\frac{P}{Q}$ for $P, Q$ coprime, output $P \cdot Q^{-1}$ modulo $10^9 + 7$.
    \end{block}

    \begin{block}{Solution}
        \begin{itemize}
            \item Let $X_i$ denote the event that the $i$-th coin yields ``heads''; $\text{Pr}[X_i] = \frac{1}{2}$. $\text{Pr}[X_1 \text{ and } \dots \text{ and } X_n] = \frac{1}{2^n}$ (independence).
            \item How do we represent $\frac{1}{2^n}$ modulo $10^9 + 7$? What is $(2^n)^{-1}$?
            \item We denote $x^{-1} \in \N$ as the \textbf{modular multiplicative inverse} of $x$ (modulo $p$), that is, any number such that $x^{-1} \cdot x \equiv 1 \modu{p}$.
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{Modular Multiplicative Inverse}
    \begin{block}{Problem}
        Assume we have a fixed modulus $p$. Given $x \in \N$ ($1 \leq x < p$), find $x^{-1}$, that is, find $y \in \N$ ($1 \leq y < p$) such that $x \cdot y \equiv 1 \modu{p}$.
    \end{block}

    \begin{block}{Solution}
        \begin{itemize}
            \item Fermat's little theorem: $a^{p-1} \equiv 1 \modu{p}$.
            \item Then $a^{p-2} = a^{-1}$. How to calculate $a^{p-2}$ (modulo $p$)?
        \end{itemize}
    \end{block}

    \begin{block}{Binary Exponentiation (Exponentiation by Squaring)}
        \begin{itemize}
            \item Say we would like to calculate $x^n$ for $n \in \N$. If $n$ is even, $n = 2k$, otherwise $n = 2k + 1$ (for some $k \in \N$).
            \item $x^n = \begin{cases} x^{2k} = (x^2)^k & n \text{ even} \\ x^{2k+1} = x \cdot x^{2k} = x \cdot (x^2)^k & n \text{ odd} \end{cases}$
            \item Time complexity: $\calO(\log{n})$ ($k$ decreases by a factor of $2$ at each step).
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile=singleslide]{More Advanced Topics}

    \begin{block}{Further Reading}
        \begin{itemize}
            \item If you would like concrete (C\verb!++!) implementations of the topics discussed along with a more detailed discussion of topics such as linear Diophantine equations, Fibonacci numbers, Euler's totient function, etc., I would recommend checking out \href{https://cp-algorithms.com/algebra/binary-exp.html}{cp-algorithms}.
            \item UT offers a full course on number theory (M28K) which usually follows \href{https://web.ma.utexas.edu/ibl/pdf/Number%20Theory%20Through%20Inquiry.pdf}{\textit{Number Theory Through Inquiry}}.
        \end{itemize}
    \end{block}

    \begin{block}{Practice Problems (from \href{https://www.eolymp.com/en/}{eolymp})}
        \begin{itemize}
            \item \href{https://www.eolymp.com/en/problems/9627}{${a^b}^c$}
            \item \href{https://www.eolymp.com/en/problems/4107}{Totient Extreme}
            \item \href{https://www.eolymp.com/en/problems/1564}{Number Theory}
        \end{itemize}
    \end{block}
    
\end{frame}

\end{document}